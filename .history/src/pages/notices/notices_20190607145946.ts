import { Component } from '@angular/core';
import { IonicPage, NavController, ToastController, ModalController, Modal, NavParams } from 'ionic-angular';
import { NoticeServiceProvider } from '../../providers/notice-service/notice-service';
import { Network } from '@ionic-native/network/ngx';

@IonicPage()
@Component({
  selector: 'page-notices',
  templateUrl: 'notices.html',
})
export class NoticesPage {
  notices: any;
  loading: boolean = false;
  loadmore: boolean = false;
  current_page: number = 1;
  last_page: number;
  next_page_url: string;
  notice_list: boolean = true;

  constructor(public navCtrl: NavController,
    private noticeService: NoticeServiceProvider,
    private modal: ModalController,
    private toastCtrl: ToastController,
    private network: Network,
    public navParams: NavParams) {
    this.getNotices(21);
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad NoticesPage');
  }

  getNotices(id) {
    if (this.network.type != 'none') {
      let post = {};
      post['get_by'] = 'all';
      post['limit'] = 7;
      this.loading = true;
      this.noticeService.index(post).subscribe((res) => {
        if (res['data']['notice_details'].length != 0) {
          this.notices = res['data']['notice_details'];
          this.loading = false;
          this.notice_list = true;
        } else {
          this.notice_list = false;
          this.loading = false;
        }
      }, (error) => {
        if (error.message) {
          const toast = this.toastCtrl.create({
            message: error.message,
            showCloseButton: true,
            closeButtonText: 'Ok'
          });
          toast.present();
        }
      });
    } else {
      let toast = this.toastCtrl.create({
        message: 'No internet connection !! Try again later !!',
        duration: 3000,
        position: 'bottom',
        cssClass: 'toast-no-connection'
      });
      toast.present(toast);
    }
  }
  ViewNoticeDetails(notice) {
    const mydata = notice;
    const myModal: Modal = this.modal.create("NoticeDetailsPage", { data: mydata });
    myModal.present();
  }
}
