import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpClientModule } from '@angular/common/http';
import { MyApp } from './app.component';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Push } from '@ionic-native/push';
import { TradeServiceProvider } from '../providers/trade-service/trade-service';
import { TradeSearchProvider } from '../providers/trade-search/trade-search';
import { Network } from '@ionic-native/network';
import { NetworkCheckProvider } from '../providers/network-check/network-check';
import { AppVersion } from '@ionic-native/app-version';
import { NotificationProvider } from '../providers/notification/notification';
import { Camera } from '@ionic-native/camera';
import { FileTransfer } from '@ionic-native/file-transfer';
import { NewBusinessRegistrationServiceProvider } from '../providers/new-business-registration-service/new-business-registration-service';
import { CategoryServiceProvider } from '../providers/category-service/category-service';
import { File } from '@ionic-native/file';
import { MyFileuploadProvider } from '../providers/my-fileupload/my-fileupload';
import { NoticeServiceProvider } from '../providers/notice-service/notice-service';
import { IonicStorageModule } from '@ionic/storage';



@NgModule({
  declarations: [
    MyApp,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Network,
    AppVersion,
    Push,
    Camera,
    FileTransfer,
    File,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    {provide:'API_URL', useValue: 'http://139.59.16.153:8080/TLRSN/tradeLicense/rest/'},
    // {provide:'API_URL', useValue: 'http://192.168.100.114:8084/tlrs/tradeLicense/rest/'},
    TradeServiceProvider,
    TradeSearchProvider,
    NetworkCheckProvider,
    NotificationProvider,
    NewBusinessRegistrationServiceProvider,
    CategoryServiceProvider,
    MyFileuploadProvider,
    NoticeServiceProvider,
  ]
})
export class AppModule {}
