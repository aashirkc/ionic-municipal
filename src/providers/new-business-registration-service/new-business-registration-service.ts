import { Injectable ,Inject} from '@angular/core';
import { HttpClient,HttpParams,HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class NewBusinessRegistrationServiceProvider {
  apiUrl: string;
  constructor(
    private http: HttpClient,
    @Inject('API_URL') apiUrl: string
  ) { 
    this.apiUrl = apiUrl;
   
  }
  index(post): Observable<any[]>{
    let myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    let Params = new HttpParams();
    for(let key in post){
      if(post.hasOwnProperty(key)){
        Params = Params.append(key, post[key]);
      }
    }
    return this.http.get(this.apiUrl + 'createTrade',{headers:myHeaders,params:Params})
    .map(
      (response) => <any[]>response,
      (error) => error
    )
  }
  store(post){
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':'multipart/form-data',
      })
    };
    return this.http.post(this.apiUrl + 'createTrade',post)
    .map(
      (response) => response,
      (error) => error
    );
    
  }
  destroy(id){
    return this.http.delete(this.apiUrl +'createTrade/'+id).map(
      (response:Response)=> response,
      (error) => error)
  }
  update(id, post): Observable<any[]>{
    return this.http.put(this.apiUrl + 'createTrade/'+id,post)
    .map(
      (response) => <any[]>response,
      (error) => error
    );
  }
  show(id){
    return this.http.get(this.apiUrl + 'createTrade/'+id)
    .map(
      (response) => response,
      (error) => error
    );
  }

}
