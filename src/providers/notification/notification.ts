import { HttpClient } from '@angular/common/http';
import { Injectable ,Inject} from '@angular/core';
import 'rxjs/add/operator/map';

@Injectable()
export class NotificationProvider {
  apiUrl: string;
    constructor(public http: HttpClient, @Inject('API_URL') apiUrl: string) {
      this.apiUrl = apiUrl;
    }
    store(gcmNumber){
      return this.http.post(this.apiUrl +'visitor?gcm_number='+ gcmNumber,{})
      .map((res)=>{
        let body = res;
        return body || { };
      },(error:Error)=> error)
     }
     get(){
      return this.http.get("http://parliament.gov.np/app_version.json")
      .map((res)=>{
        let body = res;
        return body || { };
      },(error:Error)=> error)
     }

}
