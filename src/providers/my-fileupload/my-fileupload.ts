import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

/*
  Generated class for the MyFileuploadProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MyFileuploadProvider {
  apiUrl: string;
  apiUrl2: string;
  constructor(
    private http: HttpClient,
    @Inject('API_URL') apiUrl: string
  ) {
    this.apiUrl = apiUrl;
  }

  public uploadFile(fileToUpload: File, id) {
    //note: no HttpHeaders passed as 3d param to POST!
    //So no Content-Type constructed manually.
    //Angular 4.3/4.x does it automatically.
    const _formData = new FormData();
    _formData.append('file', fileToUpload,  fileToUpload.name);
    return this.http.post(this.apiUrl + 'students/studentphotodetails?photoName=' + '&studentId=' + id, _formData);
  }

  public uploadFileCreate(fileStu: File, id) {
    const _formData = new FormData();
    _formData.append('file', fileStu, fileStu && fileStu.name);
    return this.http.post(this.apiUrl + 'uploadImage?id='+id ,_formData);
  }

}
