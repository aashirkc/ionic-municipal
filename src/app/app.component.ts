import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, ToastController, Events, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Push, PushObject, PushOptions } from '@ionic-native/push';
import { NetworkCheckProvider } from '../providers/network-check/network-check';
import { NotificationProvider } from '../providers/notification/notification';
import { AppVersion } from '@ionic-native/app-version';
import { NoticeServiceProvider } from '../providers/notice-service/notice-service';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: string = 'HomePage';

  pages: Array<any>;
  bottomPages: Array<any>;

  constructor(public platform: Platform,
    private noti: NotificationProvider,
    private alertCtrl: AlertController,
    private appVersion: AppVersion,
    public networkStatus: NetworkCheckProvider,
    private eventCtrl: Events,
    private toastCtrl: ToastController,
    private noticeService: NoticeServiceProvider,
    private push: Push,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: 'HomePage', iname: 'home' },
      { title: 'व्यवसाय व्यवस्थापन', component: 'BusinessManagementSystemPage', iname: 'paper' },
      { title: 'सुचनाहरु', component: 'NoticesPage', iname: 'notifications' },
      // { title: 'संचित व्यवसाय', component: 'SavedTradePage', iname: 'bookmark' },
      // { title: 'कार्यालय प्रयोग', component: 'OfficeUsePage', iname: 'contact' },
    ];
    this.bottomPages = [
      { title: 'FAQ', component: 'FrequentlyAskedQuestionPage', iname: 'chatbubbles' },
      { title: 'प्रतिक्रिया/सम्पर्क', component: 'FeedbackPage', iname: 'contacts' },
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {

      // Offline event
      this.eventCtrl.subscribe('network:offline', () => {
        this.displayNetworkUpdate('offline');
      });

      // Online event
      this.eventCtrl.subscribe('network:online', () => {
        this.displayNetworkUpdate('online');
      });

      this.statusBar.styleDefault();
      this.statusBar.backgroundColorByHexString('#3B5998');
      setTimeout(() => {
        this.splashScreen.hide();
      }, 300);
      const options: PushOptions = {
        android: {
          icon: 'ic_stat_government_of_nepal',
          senderID: '944292967113',
          iconColor: '#3B5998',
          sound: 'true',
          clearBadge: true,
        },
        ios: {
          alert: 'true',
          badge: true,
          sound: 'false'
        }
      };
      const pushObject: PushObject = this.push.init(options);

      pushObject.on('notification').subscribe((notification: any) => {
        console.log('Received a notification', notification);
        console.info(notification);
        if (notification.additionalData.noticeId!=0) {
          this.noticesDetails(notification.additionalData['noticeId']);
        } else {
          let prompt = this.alertCtrl.create({
            title: notification.title,
            message: notification.message
          });
          prompt.present();
        }
      });

      pushObject.on('registration').subscribe((registration: any) => {
        localStorage.setItem("gcm_number", registration.registrationId);
        console.info(registration.registrationId);
      });

      pushObject.on('error').subscribe(error => console.error('Error with Push plugin', error));
      // this.CheckUpdate();
    });
  }

  CheckUpdate() {
    this.noti.get().subscribe((response) => {
      this.appVersion.getVersionNumber().then((version) => {
        if (response['ver'] != version) {
          let confirm = this.alertCtrl.create({
            title: 'App Update Available !!',
            message: 'A New Version of Municipal App is Available In Google Play Store,Please Update Your App To Latest Version.',
            buttons: [
              {
                text: 'Cancel',
                handler: () => {
                }
              },
              {
                text: 'Update Now',
                handler: () => {
                  window.open('market://details?id=com.phoenixsolutions.lpsn', '_system');
                }
              }
            ]
          });
          confirm.present();
        }
      });
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
  displayNetworkUpdate(connectionState: string) {
    this.toastCtrl.create({
      message: `You are now ${connectionState}.`,
      duration: 5000,
      cssClass: 'toast-no-connection'
    }).present();
  }
  noticesDetails(id) {
    this.noticeService.show(id).subscribe((response) => {
      this.nav.push("NoticeDetailsPage", { data: response });
    }, (error) => {
    });

  }
}
