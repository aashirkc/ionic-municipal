import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FrequentlyAskedQuestionPage } from './frequently-asked-question';

@NgModule({
  declarations: [
    FrequentlyAskedQuestionPage,
  ],
  imports: [
    IonicPageModule.forChild(FrequentlyAskedQuestionPage),
  ],
})
export class FrequentlyAskedQuestionPageModule {}
