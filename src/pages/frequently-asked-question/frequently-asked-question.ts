import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-frequently-asked-question',
  templateUrl: 'frequently-asked-question.html',
})
export class FrequentlyAskedQuestionPage {
  FAQs:Array<any> = [];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.getFAQ();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FrequentlyAskedQuestionPage');
  }
  getFAQ(){
    this.FAQs = [
      {
        id:'१',
        title:'के व्यवसाय  दर्ता गर्न मा आफु रजिस्टर्ड हुनु पर्छ?',
        description:'प्रदैन, तपाईले "व्यवसाय  दर्ता गर्नुहोस " लिंकमा क्लिक गरि आफ्नो व्यवसाय दर्ता फारम भर्न सक्नुहुन्छ l '
      },
      {
        id:'२',
        title:'व्यवसाय  दर्ता गर्दा मैले कुनै सम्बन्धित फाईल पठाउन सक्छु ?',
        description:'तपाईले आफुले चाहेजति फाईल पठाउन सक्नुहुन्छ तर एउटा फाईल ४ एम .बि भन्दा बढी हुनुहुदैन l '
      },
      {
        id:'३',
        title:'कस्ता प्रकारका फाईलहरु पठाउन सकिन्छ ?',
        description:'तपाईले फोटो ,भिडियो ,पी .डि.एफ फाईल वा डकुमेन्ट फाईल पठाउन सक्नुहुन्छ l '
      },
      {
        id:'४',
        title:'व्यवसाय  दर्ताको लागि पास्वोर्डको आवश्कता किन पर्दछ ?',
        description:'यदि तपाई आफ्नो व्यवसायको स्थिति संबेदनसिल तथा सुरक्षित राख्न चाहानुहुन्छ भने  पास्वोर्डको आवश्कता पर्दछ र  व्यवसायको स्थिति थाहापाउन  दर्ता नम्बर को आवश्कता पर्दछ l '
      },
      {
        id:'५',
        title:'यदि मैले आफ्नो पास्वोर्ड हराएको अवस्थामा के गर्ने ?',
        description:'तपाइले "पास्वोर्ड बदल्नुहोस" लिंकमा क्लिक गरेर पास्वोर्ड बदल्न सक्नुहुन्छ l '
      }
    ]

  }
}
