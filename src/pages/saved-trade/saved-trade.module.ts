import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SavedTradePage } from './saved-trade';

@NgModule({
  declarations: [
    SavedTradePage,
  ],
  imports: [
    IonicPageModule.forChild(SavedTradePage),
  ],
})
export class SavedTradePageModule {}
