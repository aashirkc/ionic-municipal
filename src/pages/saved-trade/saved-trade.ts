import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { TradeServiceProvider } from '../../providers/trade-service/trade-service';

@IonicPage()
@Component({
  selector: 'page-saved-trade',
  templateUrl: 'saved-trade.html',
})
export class SavedTradePage {

  private savedTradeForm: FormGroup;
  tradeD: string = "tradeDetails";
  businessMans:any;
  tradeDetails:any;
  tradeHistory: any;
  showForm:boolean = true;
  loading:boolean=false;
  tradeRenewals:any;

  constructor(private formBuilder: FormBuilder,
    private toastCtrl: ToastController,
    public navCtrl: NavController,
    private tsp:TradeServiceProvider,
    public navParams: NavParams) {
    this.savedTradeForm = this.formBuilder.group({
      registered_date: ['', Validators.required],
      registration_number: [''],
    });
    let data = JSON.parse(localStorage.getItem("tradeDetails"));
    let dataHistory = JSON.parse(localStorage.getItem("tradeHistories"));
    let dataRenewals =JSON.parse(localStorage.getItem("tradeRenewal"));
    if(data){
      this.tradeDetails = data;
      this.tradeHistory = dataHistory;
      this.tradeRenewals = dataRenewals;
      this.showForm = false;
    }else{
      this.showForm = true;
    }
  }

  logForm(post) {
    post['gcm_number'] = localStorage.getItem("gcm_number");
    this.loading=true;
    this.tsp.index(post).subscribe((res)=>{
      this.loading=false;
      if(res['status_code']==200){
        this.tradeDetails = res && res['data'] && res['data']['trade_details'];
        this.tradeHistory = res && res['data'] && res['data']['trade_histories'];
        this.tradeRenewals = res && res['data'] && res['data']['trade_renewals'];
        this.showForm = false;
        localStorage.setItem("tradeDetails", JSON.stringify(this.tradeDetails));
        localStorage.setItem("tradeHistories", JSON.stringify( this.tradeHistory));
        localStorage.setItem("tradeRenewal", JSON.stringify(this.tradeRenewals));
        const toast = this.toastCtrl.create({
          message: res['message'],
          cssClass:'success-toast',
          duration: 3000,
        });
        toast.present();
      }else if(res['status_code']==400){
        const toast = this.toastCtrl.create({
          message: res['error'],
          cssClass:'error-toast',
          duration: 3000,
        });
        toast.present();
      }
    },(error)=>{
      this.loading=false;
      console.info(error);
    });
  }
  RemoveItem(){
    localStorage.removeItem("tradeDetails");
    localStorage.removeItem("tradeHistories");
    localStorage.removeItem("tradeRenewal");
    this.showForm = true;
    this.tradeDetails = null;
  }

}
