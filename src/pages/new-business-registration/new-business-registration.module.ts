import { NgModule } from '@angular/core';
import {SelectModule} from 'ng2-select';
import { IonicPageModule } from 'ionic-angular';
import { NewBusinessRegistrationPage } from './new-business-registration';

@NgModule({
  declarations: [
    NewBusinessRegistrationPage,
  ],
  imports: [
    IonicPageModule.forChild(NewBusinessRegistrationPage),
    SelectModule,
  ],
})
export class NewBusinessRegistrationPageModule {}
