import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, ToastController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { NewBusinessRegistrationServiceProvider } from '../../providers/new-business-registration-service/new-business-registration-service';
import { CategoryServiceProvider } from '../../providers/category-service/category-service';
import { File, FileEntry } from '@ionic-native/file';
import { MyFileuploadProvider } from '../../providers/my-fileupload/my-fileupload';

@IonicPage()
@Component({
  selector: 'page-new-business-registration',
  templateUrl: 'new-business-registration.html',
})
export class NewBusinessRegistrationPage {

  private gunasoForm: FormGroup;
  public base64Image: string;
  loading: boolean = false;
  isRemarkChecked: boolean = false;
  hWardNepali: string;
  hWardEnglish: String;
  tradeCategoryId: String;
  tradeSubjectId: string;
  tradeRateId: string;
  homeInRenet: boolean;
  fSelected: any;
  url: any;
  houseWardNepali: Array<any> = [
    {
      text: '१',
      id: '1'
    },
    {
      text: '२',
      id: '2'
    },
    {
      text: '३',
      id: '3'
    },
    {
      text: '४',
      id: '4'
    },
    {
      text: '५',
      id: '5'
    },
    {
      text: '६',
      id: '6'
    },
    {
      text: '७',
      id: '7'
    },
    {
      text: '८',
      id: '8'
    },
    {
      text: '९',
      id: '9'
    },
    {
      text: '१०',
      id: '10'
    },
    {
      text: '११',
      id: '11'
    },
    {
      text: '१२',
      id: '12'
    }
  ];
  houseWardEnglish: Array<any> = [
    {
      text: '1',
      id: '1'
    },
    {
      text: '2',
      id: '2'
    },
    {
      text: '3',
      id: '3'
    },
    {
      text: '4',
      id: '4'
    },
    {
      text: '5',
      id: '5'
    },
    {
      text: '6',
      id: '6'
    },
    {
      text: '7',
      id: '7'
    },
    {
      text: '8',
      id: '8'
    },
    {
      text: '9',
      id: '9'
    },
    {
      text: '10',
      id: '10'
    },
    {
      text: '11',
      id: '11'
    },
    {
      text: '12',
      id: '12'
    }
  ];
  tradeCategoryIdArray: Array<any> = [];
  tradeSubjectIdArray: Array<any> = [];
  tradeRateIdArray: Array<any> = [];

  constructor(private formBuilder: FormBuilder,
    public navCtrl: NavController,
    public camera: Camera,
    private file: File,
    public platform: Platform,
    private categoryS: CategoryServiceProvider,
    public toastCtrl: ToastController,
    private nbrs: NewBusinessRegistrationServiceProvider,
    private transfer: FileTransfer,
    private mfileUpload: MyFileuploadProvider,
    public navParams: NavParams) {
    this.gunasoForm = this.formBuilder.group({
      'firmNameNepali': ['', Validators.required],
      'firmNameEnglish': ['', Validators.required],
      'ownerNameNepali': ['', Validators.required],
      'ownerNameEnglish': ['', Validators.required],
      'fatherNameNepali': ['', Validators.required],
      'fatherNameEnglish': ['', Validators.required],
      'grandFatherNameNepali': ['', Validators.required],
      'grandFatherNameEnglish': ['', Validators.required],
      'hStreetNameNepali': [''],
      'hHouseNoNepali': [''],
      'hStreetNameEnglish': [''],
      'hHouseNoEnglish': [''],
      'steadyCapital': [''],
      'currentCapital': [''],
      'houseInRent': ['no'],
      'houseOwnerNepali': [''],
      'houseOwnerEnglish': [''],
      'monthlyRent': [''],
      'houseAddressNepali': [''],
      'houseAddressEnglish': [''],
      'sLength': [''],
      'sBreadth': [''],
      'sType': [''],
      'applicantName': [''],
      'applicantCitizenshipNo': [''],
      'applicantCitizenshipDistrict': [''],
      'applicantCitizenshipDate': [''],
      'applicantAddress': [''],
      'applicantWard': [''],
      'applicantStreetName': [''],
      'applicantHouseNo': [''],
      'applicantPhoneNo': [''],
      'applicantMobileNo': [''],
      'attachment': [''],
      'remarks': [''],
      'remarkCheck': [null]
    });
    this.getCategory();
  }

  getCategory() {
    this.categoryS.index('').subscribe((res) => {
      this.loading = false;
      if (res['status_code'] == 200) {
        if (res['data'] && res['data']['category_details']) {
          let category = [];
          for (let i = 0; i < res['data']['category_details'].length; i++) {
            let dt = {};
            dt['text'] = res['data']['category_details'][i].name;
            dt['id'] = res['data']['category_details'][i].id;
            category.push(dt);
          }
          this.tradeCategoryIdArray = category;
        }
        const toast = this.toastCtrl.create({
          message: res['message'],
          cssClass: 'success-toast',
          duration: 3000
        });
        toast.present();
      } else if (res['status_code'] == 400) {
        const toast = this.toastCtrl.create({
          message: res['error'],
          cssClass: 'error-toast',
        });
        toast.present();
      }
    }, (error) => {
      this.loading = false;
    });
  }
  takePictureCamera() {
    this.platform.ready().then(() => {
      const options: CameraOptions = {
        correctOrientation: true,
        quality: 100,
        destinationType: this.camera.DestinationType.FILE_URI,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE
      }
      this.camera.getPicture(options).then((imageData) => {
        this.base64Image = imageData;
      }, (err) => {
        // Handle error
      });
    })
  }
  takePicture() {
    this.platform.ready().then(() => {
      const options: CameraOptions = {
        sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        correctOrientation: true,
        quality: 100,
        destinationType: this.camera.DestinationType.FILE_URI,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE
      }
      this.camera.getPicture(options).then((imageData) => {
        this.base64Image = imageData;
      }, (err) => {
        // Handle error
      });
    })
  }
  upload(post, imageUrl) {
    post['hWardNepali'] = this.hWardNepali;
    post['hWardEnglish'] = this.hWardEnglish;
    post['tradeCategoryId'] = this.tradeCategoryId;
    post['tradeSubjectId'] = this.tradeSubjectId;
    post['tradeRateId'] = this.tradeRateId;
    console.info(post);
    const fileTransfer: FileTransferObject = this.transfer.create();

    let options: FileUploadOptions = {
      fileKey: 'attachment',
      chunkedMode: false,
      mimeType: "multipart/form-data",
      params: post
    }

    fileTransfer.upload(imageUrl, 'http://192.168.1.116:8088/tlrs/tradeLicense/rest/createTrade', options)
      .then((data) => {
        let json = JSON.parse(data['response']);
        let toast = this.toastCtrl.create({
          message: json.message,
          duration: 3000
        });
        toast.present();
      }, (err) => {
      });
  }
  CheckChange(e) {
    console.info(e);
    if (e == 'yes') {
      this.homeInRenet = true;
    } else {
      this.homeInRenet = false;
    }
  }

  takeHouseWardNepali($event) {
    this.hWardNepali = $event['text'];
  }

  takeHouseWardEnglish($event) {
    this.hWardEnglish = $event['text'];
  }

  takeTradeCategoryIdArray($event): void {
    this.tradeCategoryId = $event['id'];
    let data = {};
    data['ward'] = this.tradeCategoryId;
    this.categoryS.index(data).subscribe((res) => {
      this.loading = false;
      if (res['status_code'] == 200) {
        if (res['data'] && res['data']['subject_details']) {
          let subject = [];
          for (let i = 0; i < res['data']['subject_details'].length; i++) {
            let dt = {};
            dt['text'] = res['data']['subject_details'][i].name;
            dt['id'] = res['data']['subject_details'][i].id;
            subject.push(dt);
          }
          this.tradeSubjectIdArray = subject;
        }
        const toast = this.toastCtrl.create({
          message: res['message'],
          cssClass: 'success-toast',
          duration: 3000
        });
        toast.present();
      } else if (res['status_code'] == 400) {
        const toast = this.toastCtrl.create({
          message: res['error'],
          cssClass: 'error-toast',
        });
        toast.present();
      }
    }, (error) => {
      this.loading = false;
    });
  }
  takeTradeSubjectIdArray($event) {
    this.tradeSubjectId = $event['id'];
    let data = {};
    data['subjectId'] = this.tradeSubjectId;
    this.categoryS.getindex(data).subscribe((res) => {
      this.loading = false;
      if (res['status_code'] == 200) {
        if (res['data'] && res['data']['rate_details']) {
          let rate = [];
          for (let i = 0; i < res['data']['rate_details'].length; i++) {
            let dt = {};
            dt['text'] = res['data']['rate_details'][i].description;
            dt['id'] = res['data']['rate_details'][i].id;
            rate.push(dt);
          }
          console.info(rate);
          this.tradeRateIdArray = rate;
        }
        const toast = this.toastCtrl.create({
          message: res['message'],
          cssClass: 'success-toast',
          duration: 3000
        });
        toast.present();
      } else if (res['status_code'] == 400) {
        const toast = this.toastCtrl.create({
          message: res['error'],
          cssClass: 'error-toast',
        });
        toast.present();
      }
    }, (error) => {
      this.loading = false;
    });
  }

  takeTradeRateIdArray($event) {
    this.tradeRateId = $event['id'];
  }
  public fileEventS(event: any) {
    const fileSelected: File = event.target.files[0];
    this.fSelected = fileSelected;
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      reader.onload = (event: any) => {
        this.url = event.target.result;
        // console.info(this.url);
      }
      reader.readAsDataURL(event.target.files[0]);
    }
  }

  uploadFile(businnessId) {
    if (this.fSelected) {
      this.loading = true;
      this.mfileUpload.uploadFileCreate(this.fSelected, businnessId)
        .subscribe((response) => {
          console.info(response);
          this.loading = false;
        },
          (error) => {
            console.log('set any error actions...');
            this.loading = false;
          });
    }
  }

  logForm(post) {
    post['hWardNepali'] = this.hWardNepali;
    post['hWardEnglish'] = this.hWardEnglish;
    post['tradeCategoryId'] = this.tradeCategoryId;
    post['tradeSubjectId'] = this.tradeSubjectId;
    post['tradeRateId'] = this.tradeRateId;
    this.nbrs.store(post).subscribe((res) => {
      this.loading = false;
      console.info(res);
      if (res['status_code'] == 200) {
        const toast = this.toastCtrl.create({
          message: res['message'],
          cssClass: 'success-toast',
          duration: 3000,
        });
        toast.present();
        this.uploadFile(res['tradeLicenseId']);
      } else if (res['status_code'] == 400) {
        const toast = this.toastCtrl.create({
          message: res['error'],
          duration: 3000,
          cssClass: 'error-toast'
        });
        toast.present();
      }
    }, (error) => {
      this.loading = false;
    })
  }
  // private readFile(file: any, post) {
  //   const reader = new FileReader();
  //   reader.onloadend = () => {
  //     const formData = new FormData();
  //     const imgBlob = new Blob([reader.result], { type: file.type });
  //     formData.append('file', imgBlob, file.name);
  //     for (let key in post) {
  //       formData.append(key, post[key]);
  //     }

  //     this.postData(formData);
  //     console.info(formData);
  //   };
  //   reader.readAsArrayBuffer(file);
  // }
  // private postData(formData: FormData) {
  //   this.loading = true;
  //   this.nbrs.store(formData).subscribe((res) => {
  //     this.loading = false;
  //     if (res['status_code'] == 200) {
  //       const toast = this.toastCtrl.create({
  //         message: res['message'],
  //         cssClass: 'success-toast',
  //         duration: 3000,
  //       });
  //       toast.present();
  //     } else if (res['status_code'] == 400) {
  //       const toast = this.toastCtrl.create({
  //         message: res['error'],
  //         duration: 3000,
  //         cssClass: 'error-toast'
  //       });
  //       toast.present();
  //     }
  //   }, (error) => {
  //     this.loading = false;
  //   })
  // }

  CheckRemarkChange(data) {
    console.info(data);
    if (data) {
      this.isRemarkChecked = true;
    } else {
      this.isRemarkChecked = false;
    }
  }

}
