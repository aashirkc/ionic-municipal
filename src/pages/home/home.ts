import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad HomePage');
  }

  frequentlyAskedQuestions(){
    this.navCtrl.push("FrequentlyAskedQuestionPage");
  }
  feedBack(){
    this.navCtrl.push("FeedbackPage");
  }
  openWebsite(){
    window.open('http://arjundharamun.gov.np/en');
  }

  goManagementPage(){
    this.navCtrl.push("BusinessManagementSystemPage");
  }
  notices(){
    this.navCtrl.push("NoticesPage");
  }
}
