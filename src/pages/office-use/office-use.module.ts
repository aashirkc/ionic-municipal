import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OfficeUsePage } from './office-use';

@NgModule({
  declarations: [
    OfficeUsePage,
  ],
  imports: [
    IonicPageModule.forChild(OfficeUsePage),
  ],
})
export class OfficeUsePageModule {}
