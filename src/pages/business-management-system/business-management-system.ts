import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-business-management-system',
  templateUrl: 'business-management-system.html',
})
export class BusinessManagementSystemPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BusinessManagementSystemPage');
  }

  newBusinessRegistration(){
    this.navCtrl.push("NewBusinessRegistrationPage");
  }
  savedBusiness(){
    this.navCtrl.push("SavedTradePage");
  }

  businessStatus(){
    this.navCtrl.push("BusinessStatusPage");
  }
}
