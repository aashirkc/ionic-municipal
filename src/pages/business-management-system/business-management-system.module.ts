import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BusinessManagementSystemPage } from './business-management-system';

@NgModule({
  declarations: [
    BusinessManagementSystemPage,
  ],
  imports: [
    IonicPageModule.forChild(BusinessManagementSystemPage),
  ],
})
export class BusinessManagementSystemPageModule {}
