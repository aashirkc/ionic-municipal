import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BusinessStatusPage } from './business-status';

@NgModule({
  declarations: [
    BusinessStatusPage,
  ],
  imports: [
    IonicPageModule.forChild(BusinessStatusPage),
  ],
})
export class BusinessStatusPageModule {}
