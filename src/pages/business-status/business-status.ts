import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { TradeSearchProvider } from '../../providers/trade-search/trade-search';

@IonicPage()
@Component({
  selector: 'page-business-status',
  templateUrl: 'business-status.html',
})
export class BusinessStatusPage {
  tradeD: string = "tradeDetails";
  private businessStatusForm: FormGroup;
  tokens: Array<any> = [];
  tokenNumber: string = '';
  loading: boolean = false;
  showToken: boolean = true;
  tradeDetails: any;
  tradeHistory: any;
  tradeRenewals:any;

  constructor(private formBuilder: FormBuilder,
    private toastCtrl: ToastController,
    public navCtrl: NavController,
    private tss: TradeSearchProvider,
    public navParams: NavParams) {
    this.businessStatusForm = this.formBuilder.group({
      title: ['', Validators.required],
      description: [''],
    });
    this.initializeItems();
  }
  initializeItems() {
    this.tokens = JSON.parse(localStorage.getItem("tokens"));
    (this.tokens) ? this.tokens.reverse() : this.tokens = [];
  }
  logForm(post) {
    let tokens = [];
    tokens = JSON.parse(localStorage.getItem("tokens"));
    (tokens) ? tokens : tokens = [];
    let index = tokens.indexOf(post);
    if (index >= 0) {
      tokens.splice(index, 1);
    }
    if (tokens && tokens.length <= 10) {
      if (post != '' && post != null) {
        if (tokens.length == 10) {
          tokens.splice(0, 1);
        }
        tokens.push(post);
        localStorage.setItem("tokens", JSON.stringify(tokens));
      }
    }
    this.tokens = tokens.reverse();


    let data = {};
    data['token'] = post;
    this.loading = true;
    this.tss.index(data).subscribe((res) => {
      this.loading = false;
      if (res['status_code'] == 200) {
        this.showToken = false;
        this.tradeDetails = res && res['data'] && res['data']['trade_details'];
        this.tradeHistory = res && res['data'] && res['data']['trade_histories'];
        this.tradeRenewals = res && res['data'] && res['data']['trade_renewals'];
        const toast = this.toastCtrl.create({
          message: res['message'],
          cssClass:'success-toast',
          duration: 3000
        });
        toast.present();
      } else if (res['status_code'] == 400) {
        const toast = this.toastCtrl.create({
          message: res['error'],
          cssClass:'error-toast',
        });
        toast.present();
      }
    }, (error) => {
      this.loading = false;
    });
  }

  searchToken(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the searchbar
    let val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.tokens = this.tokens.filter((item) => {
        return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  takeToken(token) {
    let data = {};
    data['token'] = token;
    this.loading = true;
    this.tokenNumber = token;
    this.tss.index(data).subscribe((res) => {
      this.loading = false;
      if (res['status_code'] == 200) {
        this.showToken = false;
        this.tradeDetails = res && res['data'] && res['data']['trade_details'];
        this.tradeHistory = res && res['data'] && res['data']['trade_histories'];
        this.tradeRenewals = res && res['data'] && res['data']['trade_renewals'];
        const toast = this.toastCtrl.create({
          message: res['message'],
          cssClass:'success-toast',
          duration: 3000,
        });
        toast.present();
      } else if (res['status_code'] == 400) {
        const toast = this.toastCtrl.create({
          message: res['error'],
          duration: 3000,
          cssClass:'error-toast'
        });
        toast.present();
      }
    }, (error) => {
      this.loading = false;
    });
  }
}
