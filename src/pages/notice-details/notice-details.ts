import { Component } from '@angular/core';
import { IonicPage, NavController,ViewController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-notice-details',
  templateUrl: 'notice-details.html',
})
export class NoticeDetailsPage {
  notice:any;

  constructor(public navCtrl: NavController,
    private view:ViewController,
     public navParams: NavParams) {
  }

   ionViewWillLoad() {
    const data = this.navParams.get('data');
    this.notice = data;
  }
  closeNoticeModal(){
    this.view.dismiss();
  }

}